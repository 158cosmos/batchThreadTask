package com.thread.batchThreadTask;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BatchThreadTaskApplication {

	public static void main(String[] args) {
		SpringApplication.run(BatchThreadTaskApplication.class, args);
	}

}

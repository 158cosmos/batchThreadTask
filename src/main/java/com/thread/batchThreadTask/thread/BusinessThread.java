package com.thread.batchThreadTask.thread;

import com.thread.batchThreadTask.factory.ThreadTaskFactory;
import com.thread.batchThreadTask.inter.ThreadRobot;
import com.thread.batchThreadTask.inter.ThreadTask;
import com.thread.batchThreadTask.service.CommonSignService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.List;


/**
 * Created by Administrator on 2018/5/9.
 */
@Component
@Scope("prototype")//spring 多例
public class BusinessThread implements Runnable {

    private static final Logger LOGGER = LoggerFactory.getLogger(BusinessThread.class);
    @Override
    public void run() {

    }

    @Autowired
    CommonSignService commonSignService;
    /**
     * 创建满标合同任务，4分钟秒扫描一次
     */
     boolean fullIng= false;
    @Scheduled(cron = "0 0/4 * * * ? ")
    public void newFullScaleContract() {
        if(!fullIng) {
            fullIng = true;
            //扫描未生成满标合同的标的
            List<String> projectNos = commonSignService.listFullScaleProjectNo();
            LOGGER.info("-----------[满标合同] - 得到未生成满标合同的标的");

            //满标合同批量任务线程池
            ThreadTask<String> batchWork = ThreadTaskFactory.createBatch(new ThreadRobot<String, CommonSignService>() {
                @Override
                public void excute(List<String> queue, CommonSignService service) {
                    for (String projectNo : queue) {
                        service.getContractByProjectNo(projectNo);
                    }
                }
            }, 10, projectNos, commonSignService);

            batchWork.start();
            LOGGER.info("-----------[满标合同] - 开始批量处理满标合同");
            batchWork.doWait();
            LOGGER.info("-----------[满标合同] - 满标合同处理完成");
            fullIng = false;
        }
    }
}
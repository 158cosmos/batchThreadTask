package com.thread.batchThreadTask.service;

import java.util.List;

public interface CommonSignService {
    List<String> listFullScaleProjectNo();

    void getContractByProjectNo(String projectNo);
}

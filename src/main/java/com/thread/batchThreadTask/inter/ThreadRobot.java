package com.thread.batchThreadTask.inter;

import java.util.List;

public interface ThreadRobot<T, K> {

	/**
	 * 工作函数
	 * @param t
	 */
	void excute(List<T> queue, K param);
}